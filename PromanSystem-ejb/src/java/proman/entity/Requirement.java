/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proman.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author morph
 */
@Entity
@Table(name = "REQUIREMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Requirement.findAll", query = "SELECT r FROM Requirement r"),
    @NamedQuery(name = "Requirement.findByReqid", query = "SELECT r FROM Requirement r WHERE r.reqid = :reqid"),
    @NamedQuery(name = "Requirement.findByReqinfo", query = "SELECT r FROM Requirement r WHERE r.reqinfo = :reqinfo"),
    @NamedQuery(name = "Requirement.findByReqsize", query = "SELECT r FROM Requirement r WHERE r.reqsize = :reqsize"),
    @NamedQuery(name = "Requirement.findByPriority", query = "SELECT r FROM Requirement r WHERE r.priority = :priority")})
public class Requirement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "REQID")
    private Integer reqid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "REQINFO")
    private String reqinfo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REQSIZE")
    private int reqsize;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRIORITY")
    private int priority;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reqid")
    private Collection<Task> taskCollection;
    @JoinColumn(name = "STATUSID", referencedColumnName = "STATUSID")
    @ManyToOne(optional = false)
    private Status statusid;
    @JoinColumn(name = "PROJECTID", referencedColumnName = "PROJECTID")
    @ManyToOne(optional = false)
    private Project projectid;

    public Requirement() {
    }

    public Requirement(Integer reqid) {
        this.reqid = reqid;
    }

    public Requirement(Integer reqid, String reqinfo, int reqsize, int priority) {
        this.reqid = reqid;
        this.reqinfo = reqinfo;
        this.reqsize = reqsize;
        this.priority = priority;
    }

    public Integer getReqid() {
        return reqid;
    }

    public void setReqid(Integer reqid) {
        this.reqid = reqid;
    }

    public String getReqinfo() {
        return reqinfo;
    }

    public void setReqinfo(String reqinfo) {
        this.reqinfo = reqinfo;
    }

    public int getReqsize() {
        return reqsize;
    }

    public void setReqsize(int reqsize) {
        this.reqsize = reqsize;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @XmlTransient
    public Collection<Task> getTaskCollection() {
        return taskCollection;
    }

    public void setTaskCollection(Collection<Task> taskCollection) {
        this.taskCollection = taskCollection;
    }

    public Status getStatusid() {
        return statusid;
    }

    public void setStatusid(Status statusid) {
        this.statusid = statusid;
    }

    public Project getProjectid() {
        return projectid;
    }

    public void setProjectid(Project projectid) {
        this.projectid = projectid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reqid != null ? reqid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Requirement)) {
            return false;
        }
        Requirement other = (Requirement) object;
        if ((this.reqid == null && other.reqid != null) || (this.reqid != null && !this.reqid.equals(other.reqid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "proman.entity.Requirement[ reqid=" + reqid + " ]";
    }
    
}
