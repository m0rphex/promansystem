/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proman.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author morph
 */
@Entity
@Table(name = "TASK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Task.findAll", query = "SELECT t FROM Task t"),
    @NamedQuery(name = "Task.findByTaskid", query = "SELECT t FROM Task t WHERE t.taskid = :taskid"),
    @NamedQuery(name = "Task.findByTaskname", query = "SELECT t FROM Task t WHERE t.taskname = :taskname"),
    @NamedQuery(name = "Task.findByTaskinfo", query = "SELECT t FROM Task t WHERE t.taskinfo = :taskinfo"),
    @NamedQuery(name = "Task.findByDays", query = "SELECT t FROM Task t WHERE t.days = :days"),
    @NamedQuery(name = "Task.findByEnddate", query = "SELECT t FROM Task t WHERE t.enddate = :enddate")})
public class Task implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TASKID")
    private Integer taskid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "TASKNAME")
    private String taskname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "TASKINFO")
    private String taskinfo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DAYS")
    private int days;
    @Column(name = "ENDDATE")
    @Temporal(TemporalType.DATE)
    private Date enddate;
    @JoinColumn(name = "STATUSID", referencedColumnName = "STATUSID")
    @ManyToOne(optional = false)
    private Status statusid;
    @JoinColumn(name = "REQID", referencedColumnName = "REQID")
    @ManyToOne(optional = false)
    private Requirement reqid;
    @JoinColumn(name = "PHASEID", referencedColumnName = "PHASEID")
    @ManyToOne(optional = false)
    private Phase phaseid;
    @JoinColumn(name = "USERID", referencedColumnName = "USERID")
    @ManyToOne
    private Member1 userid;

    public Task() {
    }

    public Task(Integer taskid) {
        this.taskid = taskid;
    }

    public Task(Integer taskid, String taskname, String taskinfo, int days) {
        this.taskid = taskid;
        this.taskname = taskname;
        this.taskinfo = taskinfo;
        this.days = days;
    }

    public Integer getTaskid() {
        return taskid;
    }

    public void setTaskid(Integer taskid) {
        this.taskid = taskid;
    }

    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    public String getTaskinfo() {
        return taskinfo;
    }

    public void setTaskinfo(String taskinfo) {
        this.taskinfo = taskinfo;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Status getStatusid() {
        return statusid;
    }

    public void setStatusid(Status statusid) {
        this.statusid = statusid;
    }

    public Requirement getReqid() {
        return reqid;
    }

    public void setReqid(Requirement reqid) {
        this.reqid = reqid;
    }

    public Phase getPhaseid() {
        return phaseid;
    }

    public void setPhaseid(Phase phaseid) {
        this.phaseid = phaseid;
    }

    public Member1 getUserid() {
        return userid;
    }

    public void setUserid(Member1 userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (taskid != null ? taskid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Task)) {
            return false;
        }
        Task other = (Task) object;
        if ((this.taskid == null && other.taskid != null) || (this.taskid != null && !this.taskid.equals(other.taskid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return taskid.toString();
    }
    
}
