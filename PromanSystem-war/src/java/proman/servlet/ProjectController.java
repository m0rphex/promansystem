package proman.servlet;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import proman.beans.Member1Facade;
import proman.beans.ProjectFacade;
import proman.entity.Project;

public class ProjectController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final String INSERT_OR_EDIT = "/project.jsp";
    private static final String LIST_PROJECT = "/listProject.jsp";
    @EJB
    private ProjectFacade projectFacade;
    @EJB
    private Member1Facade member1Facade;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("delete")) {
            int projectid = Integer.parseInt(request.getParameter("projectid"));
            projectFacade.remove(projectFacade.find(projectid));
            forward = LIST_PROJECT;
            request.setAttribute("projects", projectFacade.findAll());
        } else if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            int projectid = Integer.parseInt(request.getParameter("projectid"));
            Project project = projectFacade.find(projectid);
            request.setAttribute("project", project);
        } else if (action.equalsIgnoreCase("listProject")) {
            forward = LIST_PROJECT;
            request.setAttribute("projects", projectFacade.findAll());
        } else {
            forward = INSERT_OR_EDIT;
        }
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Project project = new Project();
        project.setProjectname(request.getParameter("projectname"));
        project.setProjectinfo(request.getParameter("projectinfo"));
        project.setUserid(member1Facade.find(1));
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date parsed1 = format.parse(request.getParameter("startdate"));
            Date parsed2 = format.parse(request.getParameter("enddate"));
            java.sql.Date startdate = new java.sql.Date(parsed1.getTime());
            java.sql.Date enddate = new java.sql.Date(parsed2.getTime());
            project.setStartdate(startdate);
            project.setEnddate(enddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String projectid = request.getParameter("projectid");
        if (projectid == null || projectid.isEmpty()) {
            projectFacade.create(project);
        } else {
            project.setProjectid(Integer.parseInt(projectid));
            projectFacade.edit(project);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_PROJECT);
        request.setAttribute("projects", projectFacade.findAll());
        view.forward(request, response);
    }
}
