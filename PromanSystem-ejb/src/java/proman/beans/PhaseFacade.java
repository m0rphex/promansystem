package proman.beans;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import proman.entity.Phase;

/**
 *
 * @author morph
 */
@Stateless
public class PhaseFacade extends AbstractFacade<Phase> {
    @PersistenceContext(unitName = "PromanSystem-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PhaseFacade() {
        super(Phase.class);
    }
    
}
