package proman.servlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import proman.beans.Member1Facade;
import proman.beans.ProjectFacade;
import proman.beans.RequirementFacade;
import proman.beans.StatusFacade;
import proman.entity.Requirement;

public class RequirementController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final String INSERT_OR_EDIT = "/requirement.jsp";
    private static final String LIST_REQUIREMENT = "/listRequirement.jsp";
    private int projectid;
    @EJB
    private ProjectFacade projectFacade;

    @EJB
    private Member1Facade member1Facade;

    @EJB
    private RequirementFacade requirementFacade;

    @EJB
    private StatusFacade statusFacade;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("delete")) {
            int reqid = Integer.parseInt(request.getParameter("reqid"));
            requirementFacade.remove(requirementFacade.find(reqid));
            forward = LIST_REQUIREMENT;
            List<Requirement> requirements = new ArrayList<>();
            for(Requirement r : requirementFacade.findAll()){
                if(r.getProjectid().equals((projectFacade.find(projectid)))){
                    requirements.add(r);
                }
            }
            request.setAttribute("requirements", requirements);
        } else if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            int reqid = Integer.parseInt(request.getParameter("reqid"));
            //int projectid = Integer.parseInt(request.getParameter("projectid"));
            Requirement requirement = requirementFacade.find(reqid);
            request.setAttribute("requirement", requirement);
        } else if (action.equalsIgnoreCase("listRequirement")) {
            forward = LIST_REQUIREMENT;
            projectid = Integer.parseInt(request.getParameter("projectid"));
            List<Requirement> requirements = new ArrayList<>();
            for(Requirement r : requirementFacade.findAll()){
                if(r.getProjectid().equals((projectFacade.find(projectid)))){
                    requirements.add(r);
                }
            }
            request.setAttribute("requirements", requirements);
        } else {
            forward = INSERT_OR_EDIT;
        }
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        int projectid = Integer.parseInt(request.getParameter("projectid"));
//        System.out.println(projectid);
        Requirement requirement = new Requirement();
        requirement.setReqinfo(request.getParameter("reqinfo"));
        requirement.setStatusid(statusFacade.find(Integer.parseInt(request.getParameter("statusid"))));
        requirement.setReqsize(Integer.parseInt(request.getParameter("reqsize")));
        requirement.setPriority(Integer.parseInt(request.getParameter("priority")));
        requirement.setProjectid(projectFacade.find(projectid));
        String reqid = request.getParameter("reqid");
        if (reqid == null || reqid.isEmpty()) {
            requirementFacade.create(requirement);
        } else {
            requirement.setReqid(Integer.parseInt(reqid));
            requirementFacade.edit(requirement);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_REQUIREMENT);
        List<Requirement> requirements = new ArrayList<>();
            for(Requirement r : requirementFacade.findAll()){
                if(r.getProjectid().equals((projectFacade.find(projectid)))){
                    requirements.add(r);
                }
            }
            request.setAttribute("requirements", requirements);
        view.forward(request, response);
    }
}
