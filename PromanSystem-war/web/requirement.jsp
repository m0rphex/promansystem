<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Proman</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" type="text/css"> 
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/pure-min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            $(function() {
                $("#includenav").load("navigation.html");
            });</script>
    </head>
    <body>
        <div id="container">
            <div id="includenav"></div>
            <div id='content'>
                <form class="pure-form pure-form-stacked" method="POST" action='RequirementController'>
                    <%String action = request.getParameter("action");
                        System.out.println(action);%>
                    <fieldset>
                        <legend>Create New Requirement</legend>
                        
                        <label for="reqInfo">Description</label>
                        <input type="text" placeholder="Description" name="reqinfo" value="<c:out value="${requirement.reqinfo}" />" />
                        <label for="reqStatus">Status</label>
                        <input type="text" placeholder="Status" name="statusid" value="<c:out value="${requirement.statusid}" />" />
                        <label for="reqSize">Size</label>
                        <input type="text" placeholder="Size" name="reqsize" value="<c:out value="${requirement.reqsize}" />" />

                        <label for="reqPriority">Priority</label>
                        <input type="text" placeholder="Priority" name="priority" value="<c:out value="${requirement.priority}" />" />
                        <button type="submit" value='Submit' class="pure-button pure-button-primary">Create Requirement</button>
                    </fieldset>
                </form>
            </div>
            <div id="infopanel">Create or update a requirement</div>
        </div>
    </body>
</html>
</body>
</html>
