<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Proman</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" type="text/css"> 
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/pure-min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('#startDate').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
                jQuery('#endDate').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
            });
            $(function() {
                $("#includenav").load("navigation.html");
            });</script>
    </head>
    <body>
        <div id="container">
            <div id="includenav"></div>
            <div id='content'>
                <form class="pure-form pure-form-stacked" method="POST" action='ProjectController'>
                    <%String action = request.getParameter("action");
                        System.out.println(action);%>
                    <fieldset>
                        <legend>Create New Project</legend>
                        <label for="projectId">ID</label>
                        <input type="text" readonly="readonly" placeholder="ID" name="projectid" value="<c:out value="${project.projectid}" />" />
                        <label for="projectName">Name</label>
                        <input type="text" placeholder="Name" name="projectname" value="<c:out value="${project.projectname}" />" />
                        <label for="projectInfo">Description</label>
                        <input type="text" placeholder="Info" name="projectinfo" value="<c:out value="${project.projectinfo}" />" />
                        <label for="duration">Duration</label>
                        <input type="text" id=startDate placeholder="Start Date" name="startdate" value="<c:out value="${project.startdate}" />" />
                        <input type="text" id=endDate placeholder="End Date" name="enddate" value="<c:out value="${project.enddate}" />" />
                        <button type="submit" value='Submit' class="pure-button pure-button-primary">Create Project</button>
                    </fieldset>
                </form>
                <p><a href="RequirementController?action=listRequirement&projectid=${project.projectid}" class="pure-button pure-button-primary">Manage Requirements</a></p>
            </div>
            <div id="infopanel">Edit a project or manage Requirements</div>
        </div>
    </body>
</html>
</body>
</html>
