<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Proman</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" type="text/css"> 
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/pure-min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            $(function() {
                $("#includenav").load("navigation.html");
            });</script>
    </head>
    <body>
        <div id="container">
            <div id="includenav"></div>
            <script>

            </script>
            <div id='content'>
                <table class="pure-table">
                    <thead>
                        <tr>
                            <th>PID</th>
                            <th>#</th>
                            <th>Requirement</th>
                            <th>Status</th>
                            <th>Size</th>
                            <th>Priority</th>
                            <th colspan=2>Action</th>
                        </tr>
                    </thead>
                    <tbody>   
                        <c:forEach var="requirement" items="${requirements}">
                            <tr>
                                <td><c:out value="${requirement.projectid}" /></td>
                                <td><c:out value="${requirement.reqid}" /></td>
                                <td><c:out value="${requirement.reqinfo}" /></td>
                                <td><c:out value="${requirement.statusid}" /></td>
                                <td><c:out value="${requirement.reqsize}" /></td>
                                <td><c:out value="${requirement.priority}" /></td>
                                <td><a href="RequirementController?action=edit&reqid=<c:out value="${requirement.reqid}"/>"class="pure-button pure-button-secondary">Update</a></td>
                                <td><a href="RequirementController?action=delete&reqid=<c:out value="${requirement.reqid}"/>"class="pure-button pure-button-secodary">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <p><a href="RequirementController?action=insert" class="pure-button pure-button-primary">Add Requirement</a></p>
            </div>
            <div id="infopanel"><p>Manage requirements or Add a new one</div></p></div> 
        </div>
    </body>
</html>
