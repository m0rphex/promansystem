package proman.beans;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import proman.entity.Member1;

/**
 *
 * @author morph
 */
@Stateless
public class Member1Facade extends AbstractFacade<Member1> {
    @PersistenceContext(unitName = "PromanSystem-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Member1Facade() {
        super(Member1.class);
    }
    
}
