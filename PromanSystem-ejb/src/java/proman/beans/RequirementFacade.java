package proman.beans;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import proman.entity.Requirement;

/**
 *
 * @author morph
 */
@Stateless
public class RequirementFacade extends AbstractFacade<Requirement> {
    @PersistenceContext(unitName = "PromanSystem-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public List<Requirement> findAllWithProjectID(int projectid) {
        List<Requirement> requirements = new ArrayList<>();
        for(Requirement r : this.findAll()){
           if (r.getProjectid().equals(projectid)){
              requirements.add(r);
           }         
    }
        return requirements;
    }
    

    public RequirementFacade() {
        super(Requirement.class);
    }
    
}
