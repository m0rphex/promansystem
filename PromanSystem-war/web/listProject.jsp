<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Proman</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" type="text/css"> 
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/pure-min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('#startdate').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
                jQuery('#enddate').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
            });
            $(function() {
                $("#includenav").load("navigation.html");
            });</script>
    </head>
    <body>
        <div id="container">
            <div id="includenav"></div>
            <script>

            </script>
            <div id='content'>
                <table class="pure-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th colspan=2>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="project" items="${projects}">                           
                            <tr>
                                <td><c:out value="${project.projectid}" /></td>
                                <td><c:out value="${project.projectname}" /></td>
                                <td><c:out value="${project.projectinfo}" /></td>
                                <td><a href="ProjectController?action=edit&projectid=<c:out value="${project.projectid}"/> "class="pure-button pure-button-secondary">Update</a></td>
                                <td><a href="ProjectController?action=delete&projectid=<c:out value="${project.projectid}"/>"class="pure-button pure-button-secondary">Delete</a></td>
                            </tr>   
                        </c:forEach>
                    </tbody>
                </table>
                <p><a href="ProjectController?action=insert" class="pure-button pure-button-primary">Add Project</a></p>

            </div>
            <div id="infopanel">Select a Project or create a new one</div> 
        </div>
    </body>
</html>
