/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proman.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author morph
 */
@Embeddable
public class ParticipationPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "USERID")
    private int userid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PROJECTID")
    private int projectid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROLEID")
    private int roleid;

    public ParticipationPK() {
    }

    public ParticipationPK(int userid, int projectid, int roleid) {
        this.userid = userid;
        this.projectid = projectid;
        this.roleid = roleid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getProjectid() {
        return projectid;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userid;
        hash += (int) projectid;
        hash += (int) roleid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParticipationPK)) {
            return false;
        }
        ParticipationPK other = (ParticipationPK) object;
        if (this.userid != other.userid) {
            return false;
        }
        if (this.projectid != other.projectid) {
            return false;
        }
        if (this.roleid != other.roleid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "proman.entity.ParticipationPK[ userid=" + userid + ", projectid=" + projectid + ", roleid=" + roleid + " ]";
    }
    
}
