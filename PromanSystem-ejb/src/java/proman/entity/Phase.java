/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proman.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author morph
 */
@Entity
@Table(name = "PHASE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Phase.findAll", query = "SELECT p FROM Phase p"),
    @NamedQuery(name = "Phase.findByPhaseid", query = "SELECT p FROM Phase p WHERE p.phaseid = :phaseid"),
    @NamedQuery(name = "Phase.findByPhasename", query = "SELECT p FROM Phase p WHERE p.phasename = :phasename"),
    @NamedQuery(name = "Phase.findByStartdate", query = "SELECT p FROM Phase p WHERE p.startdate = :startdate"),
    @NamedQuery(name = "Phase.findByEnddate", query = "SELECT p FROM Phase p WHERE p.enddate = :enddate")})
public class Phase implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PHASEID")
    private Integer phaseid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "PHASENAME")
    private String phasename;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STARTDATE")
    @Temporal(TemporalType.DATE)
    private Date startdate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ENDDATE")
    @Temporal(TemporalType.DATE)
    private Date enddate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "phaseid")
    private Collection<Task> taskCollection;
    @JoinColumn(name = "PROJECTID", referencedColumnName = "PROJECTID")
    @ManyToOne(optional = false)
    private Project projectid;

    public Phase() {
    }

    public Phase(Integer phaseid) {
        this.phaseid = phaseid;
    }

    public Phase(Integer phaseid, String phasename, Date startdate, Date enddate) {
        this.phaseid = phaseid;
        this.phasename = phasename;
        this.startdate = startdate;
        this.enddate = enddate;
    }

    public Integer getPhaseid() {
        return phaseid;
    }

    public void setPhaseid(Integer phaseid) {
        this.phaseid = phaseid;
    }

    public String getPhasename() {
        return phasename;
    }

    public void setPhasename(String phasename) {
        this.phasename = phasename;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    @XmlTransient
    public Collection<Task> getTaskCollection() {
        return taskCollection;
    }

    public void setTaskCollection(Collection<Task> taskCollection) {
        this.taskCollection = taskCollection;
    }

    public Project getProjectid() {
        return projectid;
    }

    public void setProjectid(Project projectid) {
        this.projectid = projectid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (phaseid != null ? phaseid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Phase)) {
            return false;
        }
        Phase other = (Phase) object;
        if ((this.phaseid == null && other.phaseid != null) || (this.phaseid != null && !this.phaseid.equals(other.phaseid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "proman.entity.Phase[ phaseid=" + phaseid + " ]";
    }
    
}
