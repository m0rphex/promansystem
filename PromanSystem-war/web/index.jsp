<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Proman</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/jquery.ui.widget.js"></script>
        <script type="text/javascript" charset="utf-8" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" type="text/css"> 
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/pure-min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            $(function() {
                $("#includenav").load("navigation.html");
                $('#modaltrigger').leanModal({top: 110, overlay: 0.45, closeButton: ".hidemodal"});
            });
        </script>
    </head>
    <body>
        <div id="loginmodal" style="display:none;">

            <form class="pure-form pure-form-stacked" name="loginform" method="post" action="index.html">
                <fieldset>
                    <legend>Login</legend>
                    <label for="username">Username:</label>
                    <input type="text" name="username" id="username">

                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password">

                    <button type="button" id="submit" class="pure-button pure-button-primary hidemodal">Login</button>
                </fieldset>
            </form>
        </div>
        <div id="container">
            <div id="includenav"></div>
            <div id="content"><center><p>Welcome to Proman, lets get started!</p><br>
                    <a href="#loginmodal" class="pure-button pure-button-primary" id="modaltrigger">Click to Login</a></center></div>
            <div id="infopanel">Proman or the Project Management System, is a simple framework for organizing process driven teams on a day to day basis</div> 
        </div>
        <script type="text/javascript">
            $(function() {
                $('#loginform').submit(function(e) {
                    return false;
                });
            });
        </script>
    </body>
</html>
