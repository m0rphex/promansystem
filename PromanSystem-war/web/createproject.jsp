<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Proman</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" type="text/css"> 
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/pure-min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('#startdate').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
            });
            jQuery(document).ready(function() {
                jQuery('#enddate').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
            });
            $(function() {
                $("#includenav").load("navigation.html");
            });
        </script>
    </head>
    <body>
        <div id="container">
            <div id="includenav"></div>
            <script>
                $(document).ready(function() {
                    $('#submit').click(function() {
                        var projectname = $('#projectname').val();
                        var projectinfo = $('#projectinfo').val();
                        var startdate = $('#startdate').val();
                        var enddate = $('#enddate').val();
                        $.get('CreateProjectServlet', {
                            projectname: projectname,
                            projectinfo: projectinfo,
                            startdate: startdate,
                            enddate: enddate},
                        function(responseText) {
                            $('#response').text(responseText);
                        });
                    });
                });
            </script>
            <div id='content'>
                <form class="pure-form pure-form-stacked">
                    <fieldset>
                        <legend>Create New Project</legend>
                        <label for="projectname">Name</label>
                        <input id="projectname" type="text" placeholder="Name">
                        <label for="projectinfo">Description</label>
                        <input id="projectinfo" type="text" placeholder="Info">
                        <label for="duration">Duration</label>
                        <input id="startdate" type="text" placeholder="Start Date">
                        <input id="enddate" type="text" placeholder="End Date">
                        <button type="button" id="submit" class="pure-button pure-button-primary">Create Project</button>
                    </fieldset>
                </form>
            </div>
            <div id="infopanel"><p><div id="response"></div></p></div> 
        </div>
    </body>
</html>
